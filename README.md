## README

# Enable Assassins

* Re-enables the assassination option in Crusader Kings II. Paradox was kind enough to not remove the code from the base game.
* It's a very simple one-line mod, since the feature is controlled by a lua script and the game allows you to override parameters.
* v1.0

### Installation guide:

* On linux, open your ```.paradoxinteractive/Crusader Kings II/mod``` folder. The default location is ```~/.paradoxinteractive```, where ```~``` is your home directory.

* On Windows, open your ```~/Documents/Paradox Interactive/Crusader Kings II/mod``` folder. The default location is ```C:/Documents```.

* Copy the contents inside the root folder,```Enable Assassins```, which should be file ```EnableAssassins.mod``` and directory ```EnableAssassins``` into the ```...\Crusader Kings II\mod``` folder you opened above.

* Launch Crusader Kings II via Steam. Go to the mods tab and check ```Enable Assassins```.

* Enjoy assassinating characters!

### Notes:

This BitBucket version is identical to the Steam Workshop version, except for the mod file which only tells the game the path where the mod is installed. I only released it here because the Workshop was behaving strangely with my file and folder names, causing the game to not properly detect the mod, which I managed to fix by overriding the file ```descriptor.mod``` with my own to mimic the folder names for the workshop version. Unless you have a good reason not to, you should prefer the Steam Workshop version as it should work flawlessly right now.